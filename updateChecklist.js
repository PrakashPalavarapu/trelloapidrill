const getCheckItem = require("./getCheckItems");

const APIKey = "a6e2658de6baba98c4245782adca54ee";
const APIToken =
  "ATTAb37cbbde1649cccc62b791bc77e21d68b0ef8872a51e41ba603987019abc72cc68588725";

function updateAcheckList(cardId, checkItemId,state="complete") {
  return new Promise((resolve, reject) => {
    fetch(
      `https://api.trello.com/1/cards/${cardId}/checkItem/${checkItemId}?key=${APIKey}&token=${APIToken}&state=${state}`,
      {
        method: "PUT",
      }
    )
      .then((response) => {
        console.log(`Response: ${response.status} ${response.statusText}`);
        return response.text();
      })
      .then((text) => resolve(text))
      .catch((err) => reject(err));
  });
}

module.exports = updateAcheckList;
