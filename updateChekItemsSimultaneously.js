const updateAcheckList = require("./updateChecklist");
const getCheckItem = require("./getCheckItems");

let cardId = "66547a1d95d97b010b2971c6";
let checkListid = "66547a259dfe20b7e17a0e6e";

function updateAll(cardId, checkListid) {
  return new Promise((resolve, reject) => {
    let checkItemIds = [];
    getCheckItem(checkListid)
      .then((data) => {
        data.forEach((item) => {
          checkItemIds.push(item.id);
        });
        return checkItemIds;
      })
      .then((checkItemIds) => {
        let itemPromises = [];
        for (let item of checkItemIds) {
          itemPromises.push(updateAcheckList(cardId, item));
        }
        return Promise.all(itemPromises);
      })
      .then((allResult) => {
        console.log(allResult);
      })
      .catch((error) => {
        console.log(error);
      });
  });
}
updateAll(cardId, checkListid);
