const APIKey = "a6e2658de6baba98c4245782adca54ee";
const APIToken =
  "ATTAb37cbbde1649cccc62b791bc77e21d68b0ef8872a51e41ba603987019abc72cc68588725";

function createLists(listName, boardId) {
  return new Promise((resolve, reject) => {
    if (listName !== undefined && boardId !== undefined) {
      fetch(
        `https://api.trello.com/1/lists?name=${listName}&idBoard=${boardId}&key=${APIKey}&token=${APIToken}`,
        {
          method: "POST",
        }
      )
        .then((response) => {
          console.log(`Response: ${response.status} ${response.statusText}`);
          return response.text();
        })
        .then((text) => resolve(text))
        .catch((err) => reject(err));
    } else {
      reject(Error("not a valid listName or boardId"));
    }
  });
}
module.exports = createLists;
// createLists("newListCreated", "665434498d3b21415edc6085");
