const createBoard = require("./createBoard");
const createList = require("./createLists");
const createCard = require("./createCard");

function boardWithThreeLists() {
  return new Promise((resolve, reject) => {
    let creationData = {};
    createBoard("newBoard")
      .then((boardCreationStatus) => {
        creationData["boardID"] = JSON.parse(boardCreationStatus).id;
        return JSON.parse(boardCreationStatus).id;
      })
      .then((boardId) => {
        let listCreationpromises = [];
        for (let index = 0; index < 3; index++) {
          listCreationpromises.push(createList(`list${index}`, boardId));
        }
        return Promise.all(listCreationpromises);
      })
      .then((listCreationResult) => {
        let creatcardPromises = [];
        let listIds = [];
        for (list of listCreationResult) {
          creatcardPromises.push(createCard(JSON.parse(list).id));
          listIds.push(JSON.parse(list).id);
        }
        creationData["listIds"] = listIds;
        resolve(creationData);
        return Promise.all(creatcardPromises);
      })
      .catch((err) => {
        reject(err);
      });
  });
}
module.exports = boardWithThreeLists;
