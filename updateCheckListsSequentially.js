const updateAcheckList = require("./updateChecklist");
const getCheckItem = require("./getCheckItems");

let cardId = "66547a1d95d97b010b2971c6";
let checkListid = "66547a259dfe20b7e17a0e6e";

function updateAllSeqential(cardId, checkListid) {
  return new Promise((resolve, reject) => {
    let checkItemIds = [];
    getCheckItem(checkListid)
      .then((data) => {
        data.forEach((item) => {
          checkItemIds.push(item.id);
        });
        return checkItemIds;
      })
      .then((checkItemIds) => {
        for (let item of checkItemIds) {
          updateAcheckList(cardId, item,'incomplete').then((status) => {
            console.log(status);
          });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  });
}
updateAllSeqential(cardId, checkListid);
