const getCards = require("./getCards");

const APIKey = "a6e2658de6baba98c4245782adca54ee";
const APIToken =
  "ATTAb37cbbde1649cccc62b791bc77e21d68b0ef8872a51e41ba603987019abc72cc68588725";

function getCheckItem(checkListid) {
  return new Promise((resolve, reject) => {
    fetch(
      `https://api.trello.com/1/checklists/${checkListid}/checkItems?key=${APIKey}&token=${APIToken}`,
      {
        method: "GET",
      }
    )
      .then((response) => {
        console.log(`Response: ${response.status} ${response.statusText}`);
        return response.text();
      })
      .then((text) => resolve(JSON.parse(text)))
      .catch((err) => reject(err));
  });
}
module.exports = getCheckItem;
