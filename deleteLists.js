const APIKey = "a6e2658de6baba98c4245782adca54ee";
const APIToken =
  "ATTAb37cbbde1649cccc62b791bc77e21d68b0ef8872a51e41ba603987019abc72cc68588725";

function deleteLists(listId) {
  return new Promise((resolve, reject) => {
    if (listId !== undefined) {
      fetch(
        `https://api.trello.com/1/lists/${listId}/closed?key=${APIKey}&token=${APIToken}&value=true`,
        {
          method: "PUT",
        }
      )
        .then((response) => {
          console.log(`Response: ${response.status} ${response.statusText}`);
          return response.text();
        })
        .then((text) => resolve("deleted"))
        .catch((err) => reject("in catch", err));
    } else {
      reject("errror", Error);
    }
  });
}

module.exports = deleteLists;
