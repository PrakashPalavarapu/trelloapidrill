const getBoard = require("./getBoards");
const getLists = require("./getLists");
const getCards = require("./getCards");

function getAllCards(boardId) {
  return new Promise((resolve, rejcet) => {
    let allListsData = [];
    getLists(boardId)
      .then((data) => {
        data.forEach((list) => {
          allListsData.push(getCards(list.id));
        });
        return allListsData;
      })
      .then((listsPromises) => {
        return Promise.all(listsPromises);
      })
      .then((allpromises) => {
        resolve(allpromises);
      })
      .catch((error) => {
        rejcet(error);
      });
  });
}
module.exports = getAllCards;
