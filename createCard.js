const APIKey = "a6e2658de6baba98c4245782adca54ee";
const APIToken =
  "ATTAb37cbbde1649cccc62b791bc77e21d68b0ef8872a51e41ba603987019abc72cc68588725";

// `https://api.trello.com/1/cards?idList=${listId}&key=${APIKey}&token=${APIToken}`
function createCard(listId) {
  return new Promise((resolve, reject) => {
    if (listId != undefined) {
      fetch(
        `https://api.trello.com/1/cards?idList=${listId}&key=${APIKey}&token=${APIToken}`,
        {
          method: "POST",
          headers: {
            Accept: "application/json",
          },
        }
      )
        .then((response) => {
          console.log(`Response: ${response.status} ${response.statusText}`);
          return response.text();
        })
        .then((text) => console.log(text))
        .catch((err) => console.error(err));
    } else {
      reject(Error("not a valid listId"));
    }
  });
}
// createCard("665434498d3b21415edc608c");
module.exports = createCard;
