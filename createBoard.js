const apiKey = "a6e2658de6baba98c4245782adca54ee";
const apiToken =
  "ATTAb37cbbde1649cccc62b791bc77e21d68b0ef8872a51e41ba603987019abc72cc68588725";

function createBoard(boardName) {
  return new Promise((resolve, reject) => {
    if (boardName == undefined) {
      reject(Error("cannot create a board without name"));
    } else {
      fetch(
        `https://api.trello.com/1/boards/?name=${boardName}&key=${apiKey}&token=${apiToken}`,
        {
          method: "POST",
        }
      )
        .then((response) => {
          console.log(`Response: ${response.status}`);
          return response.text();
        })
        .then((text) => resolve(text))
        .catch((err) => reject(err));
    }
  });
}
module.exports = createBoard;
