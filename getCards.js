const APIKey = "a6e2658de6baba98c4245782adca54ee";
const APIToken =
  "ATTAb37cbbde1649cccc62b791bc77e21d68b0ef8872a51e41ba603987019abc72cc68588725";

function getCards(listId) {
  return new Promise((resolve, reject) => {
    if (listId != undefined) {
      fetch(
        `https://api.trello.com/1/lists/${listId}/cards?key=${APIKey}&token=${APIToken}`
      )
        .then((response) => {
          console.log(`Response: ${response.status} ${response.statusText}`);
          return response.text();
        })
        .then((text) => resolve(JSON.parse(text)))
        .catch((err) => reject(err));
    } else {
      reject(new Error("invalid listId"));
    }
  });
}
module.exports = getCards;
