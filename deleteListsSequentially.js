const boardWithThreelists = require("./boardWithThreelists");
const deleteLists = require("./deleteLists");

function deleteListsSequentially() {
  boardWithThreelists().then((data) => {
    let listIds = data.listIds;
    for (let list of listIds) {
      deleteLists(list)
        .then((data) => {
          console.log(data);
        })
        .catch((err) => {
          console.log(err);
        });
    }
  });
}
deleteListsSequentially();
