const apiKey = "a6e2658de6baba98c4245782adca54ee";
const apiToken =
  "ATTAb37cbbde1649cccc62b791bc77e21d68b0ef8872a51e41ba603987019abc72cc68588725";

function getBoard(boardID) {
  return new Promise((resolve, reject) => {
    if (boardID != undefined) {
      fetch(
        `https://api.trello.com/1/boards/${boardID}?key=${apiKey}&token=${apiToken}`
      )
        .then((rawData) => {
          return rawData.json();
        })
        .then((data) => {
          resolve(data);
        })
        .catch((error) => {
          reject(error.message);
        });
    } else {
      reject(new Error("not a valid board ID"));
    }
  });
}
module.exports = getBoard;
