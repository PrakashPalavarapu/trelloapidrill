const boardWithThreelists = require("./boardWithThreelists");
const deleteLists = require("./deleteLists");

function deleteSimultaneously() {
  boardWithThreelists().then((data) => {
    let listIds = data.listIds;
    let listPromises = [];
    for (let list of listIds) {
      listPromises.push(deleteLists(list));
    }
    Promise.all(listPromises)
      .then((deleteResult) => {
        console.log(deleteResult);
      })
      .catch((err) => {
        console.log(err);
      });
  });
}
deleteSimultaneously();
